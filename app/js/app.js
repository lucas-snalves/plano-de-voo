var app = angular.module('AppPlanoDeVoo', 
    ['ngRoute','toaster','ngMaterial', 'ngMessages', 'material.svgAssetsCache','md.data.table']);

app.config(function($mdThemingProvider, $mdIconProvider, $routeProvider, $mdDateLocaleProvider){
    
    $routeProvider
        .when('/',{
            templateUrl: 'app/partials/plano-de-voo.html',
            controller:'PlanoDeVooController'
        })

        .otherwise ({ redirectTo: '/' });

        $mdIconProvider
        .defaultIconSet("./app/assets/svg/menu.svg", 24)
        .icon("menu"       , "./app/assets/svg/menu.svg"        , 24);

        $mdThemingProvider.theme('default')
        .primaryPalette('light-blue')
        .accentPalette('blue');

        $mdDateLocaleProvider.formatDate = function(date) {
            return date ? moment(date).format('DD/MM/YYYY') : null;
        };
})

app.controller('AppController', function($scope, $mdSidenav,$location){

    $scope.toggleSidenav = function(menuId) {
		$mdSidenav(menuId).toggle();
	};

	$scope.navigateTo = function(url,pageName){
          $location.path(url);
	}

});

app.service('AppService', function($q, $mdDialog, $mdToast){

    var service = this;

    service.showConfirm = function(ev, conteudoConfirm) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title(conteudoConfirm.title)
              .textContent(conteudoConfirm.textContent)
              //.ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('Sim')
              .cancel('Não');
    
        var deferred = $q.defer();

        $mdDialog.show(confirm).then(function() {
            deferred.resolve();
        }, function() {
            deferred.reject();
        });

        return deferred.promise;
    };

    //$scope.customFullscreen = false;

    service.showAdvanced = function(ev, scope, urlTemplate) {
    
        var deferred = $q.defer();

        $mdDialog.show({
            templateUrl: urlTemplate,
            parent: angular.element(document.body),
            targetEvent: ev,
            scope: scope,
            preserveScope: true,
            clickOutsideToClose:true,
            fullscreen: true // Only for -xs, -sm breakpoints.
        }).then(function(){
            deferred.resolve();
        }, function(){

        })        
        .finally(function(){
            deferred.resolve();
        });

        return deferred.promise;

    };

    service.CloseDialog = function() {
        $mdDialog.cancel();
    };

    service.showSimpleToast = function(msg) {
        $mdToast.show(
          $mdToast.simple()
            .textContent(msg)
            .hideDelay(3000)
        );
    };

})


