app.service('TipoAeronaveService', function($q){

    var service = this;

    var listaTipoAeronave = [];
    
    listaTipoAeronave = [
        {
            id: 1,
            nome: 'Boing'
        },
        {
            id:2,
            nome: 'Jato Particular'
        }
    ]

    service.get = function(id){
        
        if(id != null)
            return listaTipoAeronave.filter(a => a.id == id);
        else
            return listaTipoAeronave;
        
    }

})