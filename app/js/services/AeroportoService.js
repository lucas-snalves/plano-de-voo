app.service('AeroportoService', function($q){

    var service = this;

    var listaAeroportos = [];
    
    listaAeroportos = [
        {
            id: 1,
            sigla: 'GUA',
            nome: 'Guarulhos'            
        },
        {
            id:2,
            sigla: 'NAT',
            nome: 'Natal'
        }
    ]

    service.get = function(id){
        
        if(id != null)
            return listaAeroportos.filter(a => a.id == id);
        else
            return listaAeroportos;
        
    }

})