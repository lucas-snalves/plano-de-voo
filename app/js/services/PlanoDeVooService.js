app.service('PlanoDeVooService', function($q){

    var service = this;

    var listaPlanosDeVoo = [];
    
    listaPlanosDeVoo = [
        {
            id: 1,
            numeroVoo: 'A3321',
            dataVoo:'2018-05-07',
            horarioVoo: '23:10',
            origemId: 1,
            destinoId: 2,            
            tipoAeronaveId: 1,
            matriculaAeronaveId: 2
        },
        {
            id:2,
            numeroVoo: 'F9878',
            dataVoo: '2019-07-10',
            horarioVoo: '05:40',
            origemId: 2,
            destinoId: 1,
            tipoAeronaveId: 2,
            matriculaAeronaveId: 1
        }
    ]

    service.get = function(id){
        
        if(id != null)
            return listaPlanosDeVoo.filter(a => a.id == id);
        else
            return listaPlanosDeVoo;
        
    }

    service.delete = function(id){

    }

    service.push = function(voo){

        listaPlanosDeVoo.push(voo);

    }
    

})