app.service('MatriculaAeronaveService', function($q){

    var service = this;

    var listaMatriculaAeronave = [];
    
    listaMatriculaAeronave = [
        {
            id: 1,
            nome: 'PT-1234'
        },
        {
            id:2,
            nome: 'PR-3333'
        }
    ]

    service.get = function(id){
        
        if(id != null)
            return listaMatriculaAeronave.filter(a => a.id == id);
        else
            return listaMatriculaAeronave;
        
    }

})