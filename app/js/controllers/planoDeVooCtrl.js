app.controller('PlanoDeVooController', function($rootScope,AppService, PlanoDeVooService, AeroportoService, TipoAeronaveService, MatriculaAeronaveService,
     $scope, toaster, $mdDialog, $q){

    $rootScope.pageTitle = "Planos de Vôo";

    $scope.listaPlanoDeVoo = [];
    $scope.listaAeroporto = [];
    $scope.listaTipoAeronave = [];
    $scope.listaMatriculaAeronave = [];

    $scope.selected = [];
    $scope.frmPlanoDeVoo = null;

    var planoDeVooSelecionado;
           
    $scope.listaPlanoDeVoo = PlanoDeVooService.get();
    $scope.listaAeroporto = AeroportoService.get();
    $scope.listaTipoAeronave = TipoAeronaveService.get();
    $scope.listaMatriculaAeronave = MatriculaAeronaveService.get();

    $scope.listaPlanoDeVoo = $scope.listaPlanoDeVoo.map(function(item){
        return {
            id: item.id,
            numeroVoo: item.numeroVoo,
            origemId: item.origemId,
            origem: $scope.listaAeroporto.filter(a => a.id == item.origemId)[0].sigla,
            destinoId: item.destinoId,
            destino: $scope.listaAeroporto.filter(a => a.id == item.destinoId)[0].sigla,
            tipoAeronaveId: item.tipoAeronaveId,
            matriculaAeronaveId: item.matriculaAeronaveId,
            dataVoo: item.dataVoo,
            horarioVoo: item.horarioVoo
        }
    })

    $scope.Remover = function(ev){
        var conteudoConfirm = {
            title:'Você tem certeza que deseja remover esse Plano de Vôo ?',
            textContent:'Uma vez excluído, não será possível reverter essa operação.'
        }
        
        var promise = AppService.showConfirm(ev, conteudoConfirm);

        promise.then(function(){
            PlanoDeVooService.delete($scope.selected[0].key).then(function(){
                AppService.showSimpleToast('Plano de Vôo removido.');
                $scope.selected = [];
            })
        }, function(){
            
        })
    }

    $scope.Detalhar = function(ev){
        
        $scope.tituloDialog = 'Atualiza Plano de Vôo';
        
        $scope.frmPlanoDeVoo = {
            numeroVoo:planoDeVooSelecionado.numeroVoo,
            origemId: planoDeVooSelecionado.origemId,
            destinoId: planoDeVooSelecionado.destinoId,
            tipoAeronaveId: planoDeVooSelecionado.tipoAeronaveId,
            matriculaAeronaveId: planoDeVooSelecionado.matriculaAeronaveId,
            dataVoo: planoDeVooSelecionado.dataVoo,
            horarioVoo: planoDeVooSelecionado.horarioVoo
        }

        AppService.showAdvanced(ev, $scope, 'app/partials/dialog-form-plano-de-voo.html');
    }

    $scope.Novo = function(ev){
        
        $scope.tituloDialog = 'Novo Plano de Vôo';
        $scope.frmPlanoDeVoo = null;
        AppService.showAdvanced(ev, $scope, 'app/partials/dialog-form-plano-de-voo.html');
    }

    $scope.SalvarPlanoDeVoo = function(){
        console.log($scope.frmPlanoDeVoo)
        return false;
        if($scope.frmPlanoDeVoo.Id === undefined || $scope.frmPlanoDeVoo.Id === null  || $scope.frmPlanoDeVoo.Id === ''){
            
            PlanoDeVooService.push($scope.frmPlanoDeVoo).then(function(){
                $scope.frmCliente = null;
                AppService.showSimpleToast('Plano de Vôo adicionado');
            })

        }
        else{

            PlanoDeVooService.put($scope.frmPlanoDeVoo).then(function(){
                $scope.frmPlanoDeVoo = null;
                AppService.showSimpleToast('Plano de Vôo atualizado');
            })

        }

        AppService.CloseDialog();

        $scope.selected = [];
        
    }

    $scope.options = {
        rowSelection: true,
        multiSelect: false,
        autoSelect: true,
        decapitate: false,
        largeEditDialog: false,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    
    $scope.deselect = function (item) {
        clienteSelecionado = null;
    };

    $scope.log = function (item) {
        planoDeVooSelecionado = item;
    };

    $scope.query = {
        order: 'numeroVoo',
        page: 1
    };

    $scope.removeFilter = function () {
        $scope.filter.show = false;
        $scope.filter.search = '';
        
        if($scope.filter.form.$dirty) {
          $scope.filter.form.$setPristine();
        }
    };

    $scope.CloseDialog = function() {
        $mdDialog.cancel();
    };

})