app.directive('menu', function(){
    return {
        templateUrl: 'app/menu/menu.html',
        restrict: 'E'
    }
})

//Verificar melhor forma de se fazer
app.directive('activeLink', function () {
    return {
        link: function (scope, element, attrs) {
            
            element.find('.nav a').on('click', function () {
                angular.element(this)
                    .parent().siblings('.active')
                    .removeClass('active');
                angular.element(this)
                    .parent()
                    .addClass('active');
            });
        }
    };
});

